#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit
{
	CLUBS,
	HEARTS,
	DIAMONDS,
	SPADES
};

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card)
{
	string suit;
	string rank;

	switch (card.Suit)
	{
	case CLUBS: suit = "Clubs";
		break;
	case SPADES: suit = "Spades";
		break;
	case HEARTS: suit = "Hearts";
		break;
	case DIAMONDS: suit = "Diamonds";
		break;
	default: "Joker";
	}

	switch (card.Rank)
	{
	case TWO: rank = "Two";
		break;
	case THREE:  rank = "Three";
		break;
	case FOUR:  rank = "Four";
		break;
	case FIVE: rank = "Five";
		break;
	case SIX: rank = "Six";
		break;
	case SEVEN: rank = "Seven";
		break;
	case EIGHT: rank = "Eight";
		break;
	case NINE: rank = "Nine";
		break;
	case TEN: rank = "Ten";
		break;
	case JACK: rank = "Jack";
		break;
	case QUEEN: rank = "Queen";
		break;
	case KING: rank = "King";
		break;
	case ACE: rank = "Ace";
		break;
	};

	cout << "The " << rank << " of " << suit;
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank) return card1;
	else if (card1.Rank < card2.Rank) return card2;
	else
	{
		if (card1.Suit > card2.Suit) return card1;
		else return card2; //else used since tie would mean same, card1 = card2
	}
}


int main()
{

	Card card1, card2;
	card1.Rank = ACE;
	card1.Suit = HEARTS;


	card2.Rank = TWO;
	card2.Suit = SPADES;

	cout << "Card 1: ";
	PrintCard(card1);
	cout << "\n";
	cout << "Card 2: ";
	PrintCard(card2);
	cout << "\n";
	cout << "High Card: ";
	PrintCard(HighCard(card1, card2));

	_getch();
	return 0;
}